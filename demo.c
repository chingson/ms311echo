#include<ms311.h>
#include<ms311sphlib.h>
#ifndef __SDCC
#define __code
#endif
#define SIM 0
#if SIM
#ifdef ECHODELAY
#undef ECHODELAY
#endif
#define ECHODELAY 4
#else
#ifndef ECHODELAY
#define ECHODELAY 15
#endif
#endif
#include "spidata.h"


extern const __code unsigned char u2lMainTabL[256];
extern const __code unsigned char u2lMainTabH[256];
extern const __code unsigned char u2lEchoTabL[256];
extern const __code unsigned char u2lEchoTabH[256];


// timer is independent from the lib
#define TMR_RLD (256-32)

// system state 
#define SYS_PLAY 1
#define SYS_REC 2
#define SYS_IDLE 0

// For EMI consideration
#define ULAWC_DEFAULT 0x18

// here we define the key related operation
#define KEYS_NOKEY 0
#define KEYS_DEB 1
#define KEYS_WAITRELEASE 2

#define KEY_CODE_PLAY 1
#define KEY_CODE_REC 2
#define KEY_CODE_PLAYREC 3
//#define KEY_CODE_PLAYREC2 4

#define KEY_WAIT 5

#define IO_REC 0x01
#define IO_PLAY 0x02
#define IO_PLAYREC 0x4
//#define IO_PLAYREC2 0x8
#define IO_KEY_ALL 0x7
// add a cap for power-on play
#define IO_CAP 0x8

// typical beep value
#define NORMAL_BEEP 0x14
#if SIM
#define BEEP_TIME1 10
#define BEEP_TIME2 8
#define REC_WAIT_TIME 10
#else
#define BEEP_TIME1 50
#define BEEP_TIME2 40
#define REC_WAIT_TIME 80
#endif


BYTE key_state;
BYTE sys_state;
BYTE last_stroke;
BYTE key_code;
BYTE key_timer;
BYTE beep_timer;
BYTE doMix;

// wait a while to enter sleep
BYTE sleep_timer;

// KEY is defined by get_key
BYTE get_key(void)
{
	if(!(IO&IO_PLAY))
		return KEY_CODE_PLAY;
	if(!(IO&IO_PLAYREC))
		return KEY_CODE_PLAYREC;
	if(!(IO&IO_REC))
		return KEY_CODE_REC;
	//if(!(IO&IO_PLAYREC2))
		//return KEY_CODE_PLAYREC2;
	return 0;
}

void init(void)
{
	// add init job here
	IO=0xFF; // all high
	IODIR=0xc0;
	IOWK=0; // deep sleep mode no use wk
	sleep_timer=KEY_WAIT;
	API_USE_ERC;
	// for EMI of ULAW, OFFSETL set to 2
	// this will induce some noise.. but is for emi only
	//OFFSETL=2;
	//RCCON=9; // use eosc
	
	api_timer_on(TMR_RLD);
}

void key_machine(void)
{
	BYTE k;	// following is key machine
	k=get_key();
	switch(key_state)
	{
		case KEYS_NOKEY:
			if(!key_code && k)
			{
				last_stroke=k;
				key_state=KEYS_DEB;
				key_timer=KEY_WAIT;
			}
			break;
		case KEYS_DEB:
			if(k!=last_stroke)
			{
				key_state=KEYS_NOKEY;
				break;
			}
			if(!--key_timer)
			{
				key_code=last_stroke;
				key_state=KEYS_WAITRELEASE;
			}
			break;
		case KEYS_WAITRELEASE:
			if(!k)
				key_state=KEYS_NOKEY;
			break;
			
	};
}

void timer_routine(void)
{
	if(!TOV)
		return ;
	TOV=0;
	if(sleep_timer)
		sleep_timer--;
	if(beep_timer)
		beep_timer--;
	// add timer jobs here
	key_machine();

}

BYTE enter_play_mode(void)
{
	BYTE try_play=0;
	
	api_set_vol(API_PAGV_DEFAULT,0x78);
#if SIM
	try_play=API_PSTARTH(simplesine);
#else
	try_play=API_PSTARTH(R1);
#endif
	if(try_play)
		sys_state=SYS_PLAY;
	return try_play; // return the result
}
void wait_beep(BYTE count)
{
	beep_timer=count;
	while(beep_timer)
	{
		timer_routine();
		if(key_state)
			api_enter_stdby_mode(0 ,0,0); // use tmr wk
		else
			api_enter_stdby_mode(IO_KEY_ALL,0,0); //use tmr+io wk

	}
}

BYTE callbackchk(void)
{
	// when start recording,
	// it checks if need abort here
	// we use only DMA wake up
	api_enter_stdby_mode(0,0,1);
	if(IO&IO_REC)
		return 1;
	return 0;
}

void enter_rec_mode(void)
{
	api_beep_start(NORMAL_BEEP);
	wait_beep(BEEP_TIME1);
	api_beep_stop();
	// after beep, we check if key released
	if(IO&IO_REC)
		return;
	api_rec_prepare(
		API_AD_OSR128,  // sample rate <12K use 128 is fine
		0xf1, // analog gain
		API_EN5K_ON // 5k ON means small gain
		); // if there is prepare, it shall be stop
	wait_beep(REC_WAIT_TIME); // wait settle down
	if(IO&IO_REC)
	{
		api_rec_stop(0); // if key released , we stop
		return;	
	}
#if SIM	
	if(!api_rec_start(6 //typical is 6
#else
	if(!api_rec_start(30 //typical is 6	
#endif	
					, 0xff // digital gain 0x80~0xff
					,R1_ULAW // Recording segment #3 defined by spidata.h
					, R1_STARTPAGE,R1_ENDPAGE
					,callbackchk)) // callback means a function to check if finish
	{
		api_rec_stop(0); // return 0 means stopped
		return;		
	}
	sys_state=SYS_REC;
	
		
}
void enter_idle_mode(void)
{
	api_play_stop();

	if(sys_state==SYS_REC) // stop from recording
	{
		api_rec_stop(1); // it will add endcode here
	
		api_beep_start(NORMAL_BEEP);
		wait_beep(BEEP_TIME2);
		api_beep_stop();
		wait_beep(BEEP_TIME2);
		api_beep_start(NORMAL_BEEP);
		wait_beep(BEEP_TIME2);
		api_beep_stop();
	}
	sys_state=SYS_IDLE;
	sleep_timer=KEY_WAIT;
}
BYTE countled;
void sys_play(void)
{
	BYTE result =api_play_job(); 
	if(!result)
		enter_idle_mode();
	else if(result==2)
	{
		/*
		if(++countled==3)
		{
			countled=0;
			IO^=0x80;
		}
		*/
		if(PWRH)
			IO&=0x7F;
		else
			IO|=0x80;
	}else
	{
		if(key_state==KEYS_NOKEY)
		{// if no key press, timer off
		  // use IO,DMA to wake up						
			api_enter_stdby_mode(IO_KEY_ALL, 0, 1);
		}else
		{ // otherwise use timer wake up
			api_enter_stdby_mode(0,0,0);
		}
	}

}

unsigned short zc_count;
BYTE zc_page;
void sys_rec(void)
{
	BYTE result;
	if(IO&IO_REC)
	{
		enter_idle_mode();
		return;
	}
	result=api_rec_job();


	
	if(!result)
		enter_idle_mode();
	else
	{
		if(result==2)
		{
			// gain control
			// we dump zc first
			zc_count+=ZC;
			// if(++zc_page>=10)
			// {
			// 	IO=0xc0;
			// 	IO=0x80&(zc_count>>8);zc_count<<=1;
			// 	IO=0x80&(zc_count>>8);zc_count<<=1;
			// 	IO=0x80&(zc_count>>8);zc_count<<=1;
			// 	IO=0x80&(zc_count>>8);zc_count<<=1;
			// 	IO=0x80&(zc_count>>8);zc_count<<=1;
			// 	IO=0x80&(zc_count>>8);zc_count<<=1;
			// 	IO=0x80&(zc_count>>8);zc_count<<=1;
			// 	IO=0x80&(zc_count>>8);zc_count<<=1;
			// 	IO=0x80&(zc_count>>8);zc_count<<=1;
			// 	IO=0x80&(zc_count>>8);zc_count<<=1;
			// 	IO=0x80&(zc_count>>8);zc_count<<=1;
			// 	IO=0x80&(zc_count>>8);zc_count<<=1;
			// 	IO=0x80&(zc_count>>8);zc_count<<=1;
			// 	IO=0x80&(zc_count>>8);zc_count<<=1;
			// 	IO=0x80&(zc_count>>8);zc_count<<=1;
			// 	IO=0x80&(zc_count>>8);zc_count<<=1;
			// 	IO=0x80&(zc_count>>8);zc_count<<=1;		
			// 	zc_page=0;
			// 	IO=0xC0;
			// }
		}
		if(IO&IO_REC)
			enter_idle_mode();
		else
			api_enter_stdby_mode(0,0,1);// use dma wake up
	}
	
}

#define TAG (*((BYTE*)0x8100))

main()
{
resetit:
	ULAWC=ULAWC_DEFAULT;
	init();
	while(1)
	{
		timer_routine();
		if(key_code)
		{
			if(sys_state!=SYS_IDLE)
				enter_idle_mode();
			else
			switch(key_code)
			{
				case KEY_CODE_REC:
					enter_rec_mode();
					break;
				case KEY_CODE_PLAY:
				case KEY_CODE_PLAYREC:
					enter_play_mode();
					break;
			}
			key_code=0;
		}
		
		if(sys_state==SYS_REC)
			sys_rec();
		else if(sys_state==SYS_PLAY)
			sys_play();
		else if(!sleep_timer && !key_state)
		{
			//IODIR=0x80;
			IODIR=0;
			IO=0;
			IOR=0xff;
			
			if(IO==0xff)
			{
				api_enter_dsleep_mode(); // this will reset
				//api_normal_sleep(IO_KEY_ALL,0,1);
			}
				/*
				{
					ADCON=0;
					LVDCON=0x20; //LPASS
					LVRCON|=8;
					LVRCON|=0x18;
					IOWKDR=0;
					IOWK=0x07;
					SYSC=4;
					SYSC=0;
					SYSC=1;
					IO=0x80;
					SYSC=4;
					SYSC=6;
				}
				*/

			
		}
		else
		{
			if(key_state)
				api_enter_stdby_mode(0 ,0,0); // use tmr wk
			else
				api_enter_stdby_mode(IO_KEY_ALL,0,0); //use tmr+io wk
			if(!TOV)
				key_machine(); // wake up by IO, we get keycode first
		}
	}
}



// modify play_job
USHORT guwStart_page;
extern USHORT api_endpage;
extern BYTE api_ulaw;
extern BYTE api_rampage;
extern BYTE api_target_vol;
//extern __code const BYTE u2l_high[256];
//extern __code const BYTE u2l_low[256];

void api_u2l_pageGainh(BYTE page);
void api_u2l_pageGainl(BYTE page);

void api_play_stop(void)
{
	DACON=0;
}
extern BYTE api_spi_index;


BYTE api_play_start(USHORT start_page, USHORT end_page, USHORT period, BYTE ulaw, BYTE dacosr)
{
	api_clear_filter_mem();
	guwStart_page=start_page;
	SPIH=start_page>>8;
	SPIM=start_page&0xff;
	SPIL=0;
	api_endpage=end_page;
	api_ulaw=ulaw;
	
	SPIOP=0x10; // clear end-code
	DMAH=0x40;
	ULAWC&=0x3f;
	ADCON=0x40;
	DAC_PH=period>>8;
	DAC_PL=period&0xff;
	OFFSETLH&=0x000f;
	ulaw &=0x7f;
		SPIOP=0x48; // read to 0x1xx
		SPIOP=0x20;
		if(SPIOP&0x80)
		{
		   
		  return 0; // fail
		}
		api_u2l_pageGainh(0);
		api_u2l_pageGainh(1);
		   
		
		DACON=dacosr|0x1a;
		

	while(DMAL==0);
	DACON|=1; // enable PA finally
	api_rampage=2; // 2xx~3xx
	return 1;
	
}




BYTE api_play_job(void)
{
	if(DMAH!=api_rampage)
	{
		// if(api_ulaw&0x80) // prevent sat
		// {
		// 	if(DCLAMP)
		// 	{
		// 		DCLAMP=1;
		// 		FILTERG-=8;
		// 	}
			
		// 	else
		// 	{
		// 		if(FILTERG!=api_target_vol)
		// 			FILTERG++;
		// 	}
			
		// }
		// i am ulaw, fixed, don't change it
		if(SPIMH-guwStart_page >= ECHODELAY)
			doMix=1;
		if(api_rampage==2)
       	{
           	SPIOP=0x48;
			if(SPIOP&0x80)
			{
				FILTERG=0;
				return 0; // end code no wait
			}
            api_u2l_pageGainh(0);
			if(doMix)
			{
				SPIMH-=ECHODELAY;
	           	SPIOP=0x48;
				api_u2l_pageGainl(0);
				SPIMH+=ECHODELAY;
	           	SPIOP=0x48;// save back
			}

        }else
		{
           	api_u2l_pageGainh(1);
			if(doMix)
			{
			SPIMH-=ECHODELAY;
           	SPIOP=0x48;
			api_u2l_pageGainl(1);
			SPIMH+=ECHODELAY;
			//SPIOP=0x48; // save back.. seems no need this step
			}
           	SPIOP=0x20;
           	if((SPIOP&0x80)||((SPIMH==api_endpage)||(SPIMH==api_endpage-1))) // -1 for remove click sound
			{
				FILTERG=0;
           		return 0;
			}
		}
		api_rampage=DMAH;
		return 2;
	}
	return 1;
}

//extern __code const BYTE u2l_high[256];
//extern __code const BYTE u2l_low[256];


    //static unsigned short exp_lut[8] = { 0, 132, 396, 924, 1980, 4092, 8316, 16764 };
 signed short origVal;
 //signed short gainVal;
// // spread for speed
// void mul21(BYTE gain) // scale doen
// {
// 	signed long origExt = origVal;
// 	signed long prod=0;
// 	// if(gain&1)
// 	// 	prod=origExt;
// 	// origExt<<=1;
// 	// if(gain&2)
// 	// 	prod+=origExt;
// 	// origExt<<=1;
// 	// if(gain&4)
// 	// 	prod+=origExt;
// 	// origExt<<=1;
// 	// if(gain&8)
// 	// 	prod+=origExt;
// 	// origExt<<=1;
// 	origExt<<=4;
// 	if(gain&0x10)
// 		prod=origExt;
// 	origExt<<=1;
// 	if(gain&0x20)
// 		prod+=origExt;
// 	origExt<<=1;
// 	if(gain&0x40)
// 		prod+=origExt;
// 	origExt<<=1;
// 	if(gain&0x80)
// 		prod+=origExt;
// 	gainVal=prod>>8;
// }
volatile BYTE ulawcode; // global
void api_u2l_pageGainh(BYTE highpage)
{
    //BYTE ulawcode;
//		unsigned char sign, exponent, mantissa;
//	        signed short sample;
    // when OF option is used, we have only 1 hardware pointer left
    // which need more careful arrangement
    // 180 => 300~400
    // 100 => 200~300
    // shift is ok
    if(highpage)
    {
        ROMPUW=0x8180;
    }
    else
    {
        ROMPUW=0x8100;
    }
    do {

		// first time
        ulawcode=ROMP;
        //

#ifdef __SDCC	
		__asm 
		LDA		_ROMPL
		SHL 
		STA     _ROMPL 
		LDA     _ROMPH 
		ROL 
		ORA     #0x80 
		STA     _ROMPH
		LDA		_ulawcode
		LDAT 	#(_u2lMainTabL + 0)
		STA  	@_ROMPINC
		LDA		_ulawcode
		LDAT 	#(_u2lMainTabH + 0)
		STA  	@_ROMPINC

		LDA _ROMPH
		SHRS 
		AND #0x83
		STA _ROMPH
		LDA _ROMPL
		ROR 
		STA _ROMPL 
		__endasm;
#endif
        ulawcode=ROMP;
        
#ifdef __SDCC	
		__asm 
		LDA		_ROMPL
		SHL 
		STA     _ROMPL 
		LDA     _ROMPH 
		ROL 
		ORA     #0x80 
		STA     _ROMPH
		LDA		_ulawcode
		LDAT 	#(_u2lMainTabL + 0)
		STA  	@_ROMPINC
		LDA		_ulawcode
		LDAT 	#(_u2lMainTabH + 0)
		STA  	@_ROMPINC

		LDA _ROMPH
		SHRS 
		AND #0x83
		STA _ROMPH
		LDA _ROMPL
		ROR 
		STA _ROMPL 
		__endasm;
#endif


    } while(ROMPL&0x7f);
}

volatile signed short ramOrigv;

void api_u2l_pageGainl(BYTE highpage)
{
    //BYTE ulawcode;
//		unsigned char sign, exponent, mantissa;
//	        signed short sample;
    // when OF option is used, we have only 1 hardware pointer left
    // which need more careful arrangement
    // 180 => 300~400
    // 100 => 200~300
    // shift is ok
	//signed short k;
    if(highpage)
    {
        ROMPUW=0x8180;
    }
    else
    {
        ROMPUW=0x8100;
    }
    do {

// twice to save loop time    
        ulawcode=ROMP;
#ifdef __SDCC	
		__asm 
		LDAT 	#(_u2lEchoTabL + 0)
		STA  	_origVal
		LDA		_ulawcode
		LDAT	#(_u2lEchoTabH +0)
		STA     (_origVal+1)
		LDA		_ROMPL
		SHL 
		STA     _ROMPL 
		LDA     _ROMPH 
		ROL 
		ORA     #0x80 
		STA     _ROMPH

		LDA  	@_ROMP
		ADD 	_origVal
		STA 	@_ROMPINC
		LDA		@_ROMP
		ADDC 	(_origVal+1)
		STA     @_ROMPINC

		
		LDA _ROMPH
		SHRS 
		AND #0x83
		STA _ROMPH
		LDA _ROMPL
		ROR 
		STA _ROMPL 
		__endasm;
#endif

		// second time
        ulawcode=ROMP;
#ifdef __SDCC	
		__asm 
		LDAT 	#(_u2lEchoTabL + 0)
		STA  	_origVal
		LDA		_ulawcode
		LDAT	#(_u2lEchoTabH +0)
		STA     (_origVal+1)
		LDA		_ROMPL
		SHL 
		STA     _ROMPL 
		LDA     _ROMPH 
		ROL 
		ORA     #0x80 
		STA     _ROMPH

		LDA  	@_ROMP
		ADD 	_origVal
		STA 	@_ROMPINC
		LDA		@_ROMP
		ADDC 	(_origVal+1)
		STA     @_ROMPINC

		
		LDA _ROMPH
		SHRS 
		AND #0x83
		STA _ROMPH
		LDA _ROMPL
		ROR 
		STA _ROMPL 
		__endasm;
#endif
        

    } while(ROMPL&0x7f);
}

